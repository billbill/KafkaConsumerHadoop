Kafka's consumer which writes the data to HDFS

NOTE: Before you do compile, you should modify the resource file src/main/resources/consumer.properties

STEP:
1. mvn clean package 
2. java/hadoop -jar target/kafka-producer-1.0-snapshot-jar-with-dependencies.jar topic_name 1 hdfs://10.112.88.213:8020 risk_data/riskService/json/riskServiceMessage.json
