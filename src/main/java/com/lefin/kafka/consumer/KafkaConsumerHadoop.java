package com.lefin.kafka.consumer;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import kafka.consumer.Consumer;
import kafka.consumer.ConsumerConfig;
import kafka.consumer.ConsumerIterator;
import kafka.consumer.KafkaStream;
import kafka.javaapi.consumer.ConsumerConnector;
import kafka.message.MessageAndMetadata;

import org.apache.log4j.Logger;

public class KafkaConsumerHadoop {

    private static Logger LOG = Logger.getLogger(KafkaConsumerHadoop.class);
    private ConsumerConfig config;
    private String topic;
    private int partitionsNum;
    private MessageExecutor executor;
    private ConsumerConnector connector;
    private ExecutorService threadPool;
    private HadoopWriter hadoopWriter;

    public KafkaConsumerHadoop(String topic, int partitionsNum, MessageExecutor executor, String hdfsUrl, String hdfsOutputFile) throws Exception{
        Properties properties = new Properties();
        properties.load(ClassLoader.getSystemResourceAsStream("consumer.properties"));
        config = new ConsumerConfig(properties);
        this.topic = topic;
        this.partitionsNum = partitionsNum;
        this.executor = executor;
        this.hadoopWriter = new HadoopWriter(hdfsUrl, hdfsOutputFile);
    }
    
    public void start() throws Exception{
        connector = Consumer.createJavaConsumerConnector(config);
        Map<String,Integer> topics = new HashMap<String,Integer>();
        topics.put(topic, partitionsNum);
        Map<String, List<KafkaStream<byte[], byte[]>>> streams = connector.createMessageStreams(topics);
        List<KafkaStream<byte[], byte[]>> partitions = streams.get(topic);
        threadPool = Executors.newFixedThreadPool(partitionsNum);
        for(KafkaStream<byte[], byte[]> partition : partitions){
            threadPool.execute(new MessageRunner(partition));
        } 
    }
        
    public void close(){
        try{
            threadPool.shutdownNow();
        }catch(Exception e){
            //
        }finally{
            connector.shutdown();
        }
    }
    
    class MessageRunner implements Runnable{
        private KafkaStream<byte[], byte[]> partition;
        
        MessageRunner(KafkaStream<byte[], byte[]> partition) {
            this.partition = partition;
        }
        
        public void run(){
            ConsumerIterator<byte[], byte[]> it = partition.iterator();
            while(it.hasNext()){
                MessageAndMetadata<byte[],byte[]> item = it.next();
                //System.out.println("partiton:" + item.partition());
                //System.out.println("offset:" + item.offset());
                //executor.execute(new String(item.message()));//UTF-8
                try {
                    hadoopWriter.WriteToHDFS(new String(item.message()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    interface MessageExecutor {
        public void execute(String message);
    }
    
    /**
     * @param args
     */
    public static void main(String[] args) {
	if (args.length < 4) {
            System.err.println("Usage: KafkaConsumerHadoop <topics> <numThreads> <hdfsUrl> <hdfsOutputFile>");
            System.exit(1);
    	}

	    String _topic = args[0];
        int _numThreads = Integer.parseInt(args[1]);
        String _hdfsUrl = args[2];
        String _hdfsOutputFile = args[3];

        KafkaConsumerHadoop consumer = null;
        try{
            MessageExecutor executor = new MessageExecutor() {
                public void execute(String message) {
                    System.out.println(message);
                }
            };
            consumer = new KafkaConsumerHadoop(_topic, _numThreads, executor, _hdfsUrl, _hdfsOutputFile);
            consumer.start();
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            //if(consumer != null){
            //    consumer.close();
            //}
        }
    }
}
