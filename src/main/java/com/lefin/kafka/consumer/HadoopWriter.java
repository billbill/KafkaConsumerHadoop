package com.lefin.kafka.consumer;

import java.io.IOException;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import org.apache.log4j.Logger;

public class HadoopWriter {

    private static final String encoding = "UTF-8";
    private static Logger LOG = Logger.getLogger(KafkaConsumerHadoop.class);
    private Configuration config;
    private FileSystem hdfs;
    private Path path;

    public HadoopWriter(String url, String OutputFile) throws IOException, URISyntaxException{
        this.config = new Configuration();
        this.config.set("fs.hdfs.impl", org.apache.hadoop.hdfs.DistributedFileSystem.class.getName());
        this.config.set("fs.file.impl", org.apache.hadoop.fs.LocalFileSystem.class.getName());
        this.hdfs = FileSystem.get(new URI(url), this.config);
        this.path = new Path(OutputFile);
    }

    public void WriteToHDFS(String words) throws IOException{
        FSDataOutputStream out;
	    if (this.hdfs.exists(this.path)) {
            out = this.hdfs.append(this.path);
	    }
	    else {
            out = this.hdfs.create(this.path);
	    }
        // Way 1
        SaveFSDataOutputStream(out,words);

        // Way 2
        ////out.writeBytes(words);
        //out.write(words.getBytes(encoding));
        //out.close();
	    ////CloseFileSystem();
    }

    public void SaveFSDataOutputStream(FSDataOutputStream fsDataOutputStream, String str) throws IOException{
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fsDataOutputStream, encoding);
        BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
        bufferedWriter.write(str+'\n');
        bufferedWriter.close();

        outputStreamWriter.close();
        fsDataOutputStream.close();
    }

    public void CloseFileSystem() throws IOException{
        LOG.info("Closing filesystem and stopping ...");
	    this.hdfs.close();
    }
}
